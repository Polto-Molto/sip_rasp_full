#include <pjsua2.hpp>
#include <iostream>
#include <string>
#include <memory>
#include <wiringPi.h>
#include <stdio.h>

using namespace pj;
using namespace std;

#define PJ_IS_LITTLE_ENDIAN 1
#define PJ_IS_BIG_ENDIAN 0

class MyCall : public Call
{
public:
  MyCall(Account &acc, int call_id = PJSUA_INVALID_ID)
  : Call(acc, call_id)
  { }

  ~MyCall()
  { }


  // Notification when call's state has changed.
  virtual void onCallState(OnCallStateParam &prm)
  {
    CallInfo ci = getInfo();
    if (ci.state == PJSIP_INV_STATE_DISCONNECTED) {
        /* Delete the call */
        delete this;
	try {
	   } catch(Error& err) {
	}
    }
  }
/*
  virtual void onCallSdpCreated(OnCallSdpCreatedParam &prm)
  {
    CallInfo ci = getInfo();
	cout << " ***************************************** " <<endl;
	cout << ci.state << endl;
        cout << " *******************************************" << endl;
        CallOpParam prm1;
        prm1.statusCode = PJSIP_SC_RINGING;
        answer(prm1);
        cout << " ***************************************** " <<endl;
        cout << ci.state << endl;
        cout << " *******************************************" << endl;

//    CallInfo ci = getInfo();

            AudioMediaPlayer player;
        AudioMedia& play_med = Endpoint::instance().audDevManager().getPlaybackDevMedia();
        player.createPlayer("ring.wav");
        player.startTransmit(play_med);
        
  }
*/
  // Notification when call's media state has changed.
  virtual void onCallMediaState(OnCallMediaStateParam &prm)
  {
    CallInfo ci = getInfo();

    // Iterate all the call medias
    for (unsigned i = 0; i < ci.media.size(); i++) {
        if (ci.media[i].type==PJMEDIA_TYPE_AUDIO && getMedia(i)) {
            AudioMedia *aud_med = (AudioMedia *)getMedia(i);

            // Connect the call audio media to sound device
            AudDevManager& mgr = Endpoint::instance().audDevManager();
            aud_med->startTransmit(mgr.getPlaybackDevMedia());
            mgr.getCaptureDevMedia().startTransmit(*aud_med);
        }
    }
  }

  virtual void onDtmfDigit(OnDtmfDigitParam &prm)
  {
    int key_num = stoi(prm.digit) ;
    if(digitalRead (key_num)  > 0)
    {
      cout << "call center enter key to close gate num :" << key_num << endl;
      digitalWrite (key_num, 0) ;
    }
    else
    {
      cout << "call center enter key to open gate num :" << key_num << endl;
      digitalWrite (key_num, 1) ;
    }
  }

  virtual void onCallTsxState(OnCallTsxStateParam &prm)
  {

    /*  "application/dtmf-relay" */
    string str = prm.e.body.tsxState.src.rdata.wholeMsg;
    int i = 0; 
    int key_num = -1;
    i = str.find("dtmf-relay");
    if(i > 0)
    {
      i = str.find("Signal=");
      key_num = std::stoi(str.substr(i+7,1));
      if(digitalRead (key_num)  > 0)
      {
        cout << "call center enter key to close gate num :" << key_num << endl;
        digitalWrite (key_num, 0) ;
      }
      else
      {
        cout << "call center enter key to open gate num :" << key_num << endl;
        digitalWrite (key_num, 1) ;
      }
    }

  }

};

class MyAccount : public Account
{
public:
    MyAccount() {}
    ~MyAccount() {}

    virtual void onRegState(OnRegStateParam &prm)
    {
        AccountInfo ai = getInfo();
        cout << (ai.regIsActive? "*** Register: code=" : "*** Unregister: code=")
             << prm.code << endl;
    }

    virtual void onIncomingCall(OnIncomingCallParam &iprm)
    {
        Call *call = new MyCall(*this,iprm.callId);

        CallOpParam prm;
        prm.statusCode = PJSIP_SC_OK;
        call->answer(prm);
   }

};


int main(int argc , char* argv[])
{
    wiringPiSetup () ;
    pinMode (0,OUTPUT) ;
    pinMode (1,OUTPUT) ;
    pinMode (2,OUTPUT) ;
    pinMode (3,OUTPUT) ;

   Endpoint ep;

    ep.libCreate();

    // Initialize endpoint
    EpConfig ep_cfg;
    ep_cfg.medConfig.hasIoqueue = false;
    ep.libInit( ep_cfg );

    // Create SIP transport. Error handling sample is shown
    TransportConfig tcfg;
    tcfg.port = 0;

    try {
        ep.transportCreate(PJSIP_TRANSPORT_TCP, tcfg);
        ep.transportCreate(PJSIP_TRANSPORT_UDP, tcfg);
    } catch (Error &err) {
        std::cout << err.info() << std::endl;
        return 1;
    }

    // Start the library (worker threads etc)
    ep.libStart();
    // Add account
    AccountConfig acc_cfg;

    string uri = "sip:"+string(argv[2])+"@"+string(argv[1]);
    acc_cfg.idUri = uri;

    string reg = "sip:"+string(argv[1]);
    acc_cfg.regConfig.registrarUri = reg;

    acc_cfg.sipConfig.authCreds.push_back( AuthCredInfo("digest", "*",
                                                        string(argv[2]), 0, string(argv[3])) );


    MyAccount *acc = new MyAccount;
    try {
        acc->create(acc_cfg);
    } catch(Error& err) {
        cout << "Account creation error: " << err.info() << endl;
    }

    pj_thread_sleep(10000);

    if (argc > 4)
    {
    	// Make outgoing call
    	Call *call = new MyCall(*acc);
    	CallOpParam prm(true); // Use default call settings
    	try {
      		string call_str = "sip:"+string(argv[4])+"@"+string(argv[1]);
        	call->makeCall(call_str, prm);
    	} catch(Error& err) {
        	cout << err.info() << endl;
    	}

    	// Hangup all calls
    	pj_thread_sleep(80000);
    	ep.hangupAllCalls();
    	pj_thread_sleep(4000);
    }
    // Destroy library
    std::cout << "*** PJSUA2 SHUTTING DOWN ***" << std::endl;
   while(1);
    return 0;
}
