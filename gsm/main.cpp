#include <iostream>
#include <thread>
#include <wiringSerial.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

using namespace std;

void readGSMData(int fd)
{
  char buf[128];
 int i = 0;
int ch = 0;
  while(1)
  {
    int n = serialDataAvail(fd) ;
    if(n> 0 )
    {
      while(( ch = serialGetchar(fd)) != 13 )
      {
       buf[i] = char(ch);
       i++;
      }
    }
    else
    {
     if (i > 0)
      {
      cout << "received : " << buf << endl;
      for(int z =0 ;z <128 ; z++)
        buf[z] = '\0';
      i = 0;
      }
    }
  }
}

void writeGSMData(int fd)
{
  char buf[60];
  while(1)
  {
    for(int i =0 ;i <60 ; i++)
      buf[i] = '\0';
    fgets(buf,60,stdin);
    int n = strlen(buf);
   buf[n]='\0';
   buf[n+1]= '\r';
   buf[n+2]= '\n';
    write(fd,buf,n+2);
    cout << n << endl;
//    write(fd,"\n\0",2);
  }
}


int main(int argc , char* argv[])
{
  int s = serialOpen ("/dev/ttyS0" , 115200);
  std::thread reading (readGSMData , s);
  std::thread writing (writeGSMData , s);

  while(1);
  return 0;
}
